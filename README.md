# Hispano Sozluk

It is build on top of [Pup](https://cleverbeagle.com/pup)

# Structure

Application is basically divided into two parts;

1. Conjugations
2. Dictionary
  * Turkish

The biggest problem of the data lies in this part since the Turkish entries would have to be manually checked in case using Google API is not an option.
  * Spanish
  * English
