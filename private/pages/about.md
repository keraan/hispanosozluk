### Nedir ?

Hispano Sözlük'ün temel amacı İspanyolca kelimeleri Türkçe karşılıklarıyla beraber bir çatı altında toplamaktır.
Günümüzde bir çok birey İspanyolca öğrenmeyi tercih etmiştir. Fakat yetersiz olan kaynaklardan dolayı bilgiye erişim
kısıtlı kalmaktadır. Bizim amacımız Türkçe, İspanyolca ve İngilizce dillerinde örneklerle beraber bir veritabanı oluşturup bunu bireylerin kullanıma sunmaktır.

### Neden ?

Büyükçe bir neden var aslında ortanda. O da bilgi eksikliği. Yabancı kaynaklardan bilgiye ulaşabilmemize rağmen toplumun büyük bir bölümü bundan mahrum kalıyor. İspanyolca öğrenen birinin nedense İngilizce biliyor gibi genel kabul görülen bir yaklaşım var. Bu oluşum bu görüşten uzaklaşarak, komunite bazında destek alarak üyelerin katkılarıyla oluşturulan bir platform haline gelmeyi planlamaktadır. Bilgi dediğimiz şey paylaştıkça çoğalır sözünden yola çıkarak umarım hep beraber sahip olduğumuz bilgiyi paylaşır ve daha büyük kitlelerin kullanımına sunarız.

### Hispano Sözlük ?

Neden İspanyolca sözlük değil de Hispano gibi bir soru varsa kafanızda haklısınız. Aynı şekilde benim de kafamda bu soru vardı yıllarca. Castellano ve Español farkından yola çıkmamız gerekiyor. İspanya'da insanlar konuştukları dile, özellikle Madrid bölgesinde, Castellano diyorlar. Bu bir nevi Castilla dili demek gibi. Latin Amerika ve İspanya arasında konuşulan dialektler arasında bir uçurum bulunmasa da bir çok farklılık mevcut. Buradaki amaç bu farklılıkların farkında olup ona göre bilgi aktarımı yapmak.
