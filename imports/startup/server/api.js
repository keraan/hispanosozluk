import '../../api/Documents/methods';
import '../../api/Documents/server/publications';

import '../../api/Dictionary/methods';
import '../../api/Dictionary/server/publications';

import '../../api/Conjugations/methods';
import '../../api/Conjugations/server/publications';

import '../../api/OAuth/server/methods';

import '../../api/Users/server/methods';
import '../../api/Users/server/publications';

import '../../api/Utility/server/methods';
