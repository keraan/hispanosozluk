import React from 'react';
import Page from '../Page/Page';

const About = () => (
  <div>
    <Page
      title="Hispano Sözlük"
      subtitle="Hakkında"
      page="about"
    />
  </div>
);

export default About;
