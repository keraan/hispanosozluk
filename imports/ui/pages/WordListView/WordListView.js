import React from 'react';
import PropTypes from 'prop-types';
import { ListGroup, ListGroupItem, ButtonToolbar, ButtonGroup, Button } from 'react-bootstrap';
import { createContainer } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor';
import Dictionary from '../../../api/Dictionary/Dictionary';
import NotFound from '../NotFound/NotFound';
import Loading from '../../components/Loading/Loading';


const renderDocument = (word,history) => (word ? (
  <div className="ViewDocument">
    <div className="page-header clearfix">
      <div className='col-md-6'>
      <ListGroup>
      <ListGroupItem header="Türkçe">{word.turkish}</ListGroupItem>
      <ListGroupItem header="İspanyolca">{word.spanish}</ListGroupItem>
      <ListGroupItem header="İngilizce" >{word.english}</ListGroupItem>
      <ListGroupItem header="Tür">{word.type}</ListGroupItem>
      <ListGroupItem header="Örnek" bsStyle="success">{word.examples}</ListGroupItem>
    </ListGroup>
    <ButtonToolbar className="pull-right">
    <ButtonGroup bsSize="small">
      <Button bsStyle="warning" onClick={() => history.goBack()}>Geri Dön</Button>
    </ButtonGroup>
  </ButtonToolbar>
    </div>
    </div>
  </div>
) : <NotFound />);

const WordListView = ({ loading, word,history}) => (
  !loading ? renderDocument(word,history) : <Loading />
);

WordListView.propTypes = {
  loading: PropTypes.bool.isRequired,
  word: PropTypes.object,
};

export default createContainer(({ match }) => {
  const word = match.params.word;
  const subscription = Meteor.subscribe('findWords.userListView', word);

  return {
    loading: !subscription.ready(),
    word: Dictionary.findOne({ turkish: word }),
  };
}, WordListView);
