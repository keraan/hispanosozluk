import React from 'react';
import { Meteor } from 'meteor/meteor';
import { createContainer } from 'meteor/react-meteor-data';
import { Table, Alert, Button, ButtonToolbar } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { Bert } from 'meteor/themeteorchef:bert';
import Loading from '../../components/Loading/Loading';
import './Wordlist.scss';

const handleRemove = (favID) => {
  if (confirm('Gerçekten silmek istiyor musunuz?')) {
    Meteor.call('user.removeBookmark', favID, (error) => {
      if (error) {
        Bert.alert(error.reason, 'danger');
      } else {
        Bert.alert('Document deleted!', 'success');
      }
    });
  }
};

class Wordlist extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      edit: false,
    };
  }
  render() {
    return (
      <div>
        {!this.props.loading ?
          <div className="container">
            <div className="row">
              <div className="col-md-8">
                <h1>Kelime Listem</h1>
              </div>
              <div className="col-md-4">
                <ButtonToolbar>
                  <Button bsStyle="info">Pratik Yap</Button>
                  <Button bsStyle="info">PDF <i className="fa fa-download" /></Button>
                  <Button bsStyle="warning" onClick={() => this.setState({ edit: !this.state.edit })}>Düzenle <i className="fa fa-edit" /></Button>
                  <Button bsStyle="warning">Paylaş <i className="fa fa-share" /></Button>
                </ButtonToolbar>
              </div>
            </div>
            <div>
              {this.props.user.favorites.length ? <Table responsive>
                <thead>
                  <tr>
                    <th>Kelime</th>
                    <th />
                  </tr>
                </thead>
                <tbody>
                  {this.props.user.favorites.map(fav => (
                    <tr key={fav.wordID}>
                      <td>{fav.word}</td>
                      <td >
                        <Link className="btn btn-success pull-right" to={`/words/user/list/${fav.word}`}> Gözat </Link>
                      </td>
                      {this.state.edit ? <td className="wordlist__actions">
                        <Button bsStyle="warning" className="pull-right"
                          onClick={() => handleRemove(fav.wordID)}
                        > Sil <i className="fa fa-trash-o" /> </Button>
                      </td> : null}
                    </tr>
                  ))}
                </tbody>
              </Table> : <Alert bsStyle="warning">Henüz bir kelime eklememişsiniz :(</Alert>}
            </div>
          </div> : <Loading />}
      </div>
    );
  }
}

export default createContainer(() => {
  const subscription = Meteor.subscribe('users.getBookmarks');
  return {
    loading: !subscription.ready(),
    user: Meteor.user(),
  };
}, Wordlist);
