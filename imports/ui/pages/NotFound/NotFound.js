import React from 'react';
import { Alert } from 'react-bootstrap';

import './NotFound.scss';

const NotFound = () => (
  <div className="boo__container">
    <div className="boo__wrapper">
      <div className="boo">
        <div className="face" />
      </div>
      <div className="shadow" />

      <h1>Kusura bakma!</h1>
      <p>
        <Alert className="boo__alert" bsStyle={'info'} > {window.location.pathname} </Alert>
        malasef sistemizde bulunmuyor :(
      </p>
    </div>
  </div>
);

export default NotFound;
