import React, { Component } from 'react';
import { Button } from 'react-bootstrap';

import Results from './Results';
import './Search.scss';

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchValue: false,
      language: 'turkish',
      getResults: false,
    };
    this.setSearchValue = this.setSearchValue.bind(this);
    this.setLanguage = this.setLanguage.bind(this);
    this.getResults = this.getResults.bind(this);
  }
  setSearchValue(e) {
    this.setState({
      searchValue: e.target.value,
    }, () => console.log(this.state.searchValue));
  }
  setLanguage(e) {
    this.setState({
      language: e.target.value,
    }, () => console.log(this.state.language));
  }
  getResults() {
    this.setState({
      getResults: this.state.searchValue,
    });
  }
  render() {
    return (
      <div>
        <div className="search__container">
          <input
            className="search-bar"
            type="text"
            id="search-bar"
            placeholder="Hangi kelimeyi ariyorsunuz ?"
            onChange={this.setSearchValue}
          />
          <select className="option__bar" onChange={this.setLanguage}>
            <option value="turkish" >Türkçe - İspanyolca</option>
            <option value="spanish" >İspanyolca - Türkçe</option>
            <option value="english" >İngilizce - İspanyolca</option>
          </select>
          <Button onClick={this.getResults}>Çevir</Button>
        </div>
        {this.state.getResults ?
          <Results
            getResults={this.state.getResults}
            language={this.state.language}
            searchValue={this.state.getResults}
          /> :
                  null
        }
      </div>
    );
  }
}

export default Search;
