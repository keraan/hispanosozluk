import React from 'react';
import { Link } from 'react-router-dom';
import { Meteor } from 'meteor/meteor'
import PropTypes from 'prop-types';
import { ListGroup, ListGroupItem, Button, Popover, OverlayTrigger, ButtonToolbar } from 'react-bootstrap';
import './ResultWord.scss';

const popoverTop = ({ explanation }) => (
  <Popover id="popover-positioned-top" title="Açıklama">
    <strong>{explanation}</strong>
  </Popover>
);

const checkBookmarks = (favorites, wordID) => {
  const items = [];
  for (let i = 0; i < favorites.length; i += 1) {
    items.push(favorites[i].wordID);
  }
  return items.includes(wordID);
};

const ResultWord = ({ turkish, english, spanish, examples, wordID, type, explanation, handleBookmarks, favorites }) => (
  <div className="result__word container">
    <div className="row">
      <div className="col-md-8">
        <ListGroup>
          <ListGroupItem header="Türkçe">{turkish}</ListGroupItem>
          <ListGroupItem header="İspanyolca">{spanish}</ListGroupItem>
          <ListGroupItem header="İngilizce" >{english} <i className="fa fa-sound" /></ListGroupItem>
          <ListGroupItem header="Tür">{type}</ListGroupItem>
          <ListGroupItem header="Örnek" bsStyle="success">{examples}</ListGroupItem>
        </ListGroup>
      </div>
      {Meteor.user() ? <div className="col-md-2">
        <ButtonToolbar>
        {checkBookmarks(favorites, wordID) ? <Button
          bsStyle="warning"
          block
          disabled
        >Listenizde var.</Button> :
        <Button
            bsStyle="warning"
            block
            onClick={() => handleBookmarks(turkish, wordID)}
          >Listeme Ekle</Button> }
        {explanation ?
          <OverlayTrigger trigger="click" placement="right" overlay={popoverTop({ explanation })}>
            <Button bsStyle="info" block>Bilgi</Button>
          </OverlayTrigger>
          : null}
          <Button
              bsStyle="danger"
              block
              onClick={() => handleBookmarks(turkish, wordID)}
            >Rapor Et</Button>
          </ButtonToolbar>
      </div> : null }
    </div>
  </div>
);
ResultWord.propTypes = {
  turkish: PropTypes.string.isRequired,
  english: PropTypes.string,
  spanish: PropTypes.string,
  date: PropTypes.string,
  examples: PropTypes.string,
  wordID: PropTypes.string,
  favorites: PropTypes.array,
};

ResultWord.defaultProps = {
  favorites: []
};

export default ResultWord;
