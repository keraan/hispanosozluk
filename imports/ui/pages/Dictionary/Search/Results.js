import React, { Component } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor';
import PropTypes from 'prop-types';
import { Typeahead } from 'react-bootstrap-typeahead';
import Loading from '../../../components/Loading/Loading';

import Dictionary from '../../../../api/Dictionary/Dictionary';
import ResultWord from './ResultWord';


class Results extends Component {
  constructor(props) {
    super(props);
    this.renderWords = this.renderWords.bind(this);
    this.handleBookmarks = this.handleBookmarks.bind(this);
  }
  handleBookmarks(word, wordID) {
    Meteor.call('user.bookmarkWord', word, wordID);
  }
  renderWords() {
    return this.props.results.map((word, index) => (
      <ResultWord
        key={index}
        turkish={word.turkish}
        english={word.english}
        spanish={word.spanish}
        examples={word.examples}
        date={word.createdAt}
        wordID={word._id._str}
        type={word.type}
        explanation={word.explanation}
        handleBookmarks={this.handleBookmarks}
        favorites={this.props.user.favorites ? this.props.user.favorites : []}
      />));
  }
  render() {
    return (
      <div className="wrapper">
        <div className="results__container">
          {this.renderWords()}
        </div>
      </div>
    );
  }
}

export default createContainer((props) => {
  Meteor.subscribe('findWords', props.getResults, props.language);
  return {
    user: Meteor.user(),
    results: props.language === 'turkish' ?
      Dictionary.find({ turkish: { $regex: props.getResults, $options: 'i' } }).fetch() :
      Dictionary.find({ spanish: { $regex: props.getResults, $options: 'i' } }).fetch(),
  };
}, Results);
