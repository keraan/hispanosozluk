import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import moment from 'moment';
import { ListGroup, ListGroupItem } from 'react-bootstrap';
import './ResultWord.scss';

const ResultWord = ({ turkish, mood, tense, infinitive, infinitiveEnglish, firstPersonSingular, secondPersonSingular,
  thirdPersonSingular, firstPersonPlural, secondPersonPlural, thirdPersonPlural }) => (
    <div className="col-md-4">
    <ListGroup>
        <ListGroupItem header="Zaman" bsStyle="info">{tense}</ListGroupItem>
        <ListGroupItem header="Kip">{mood}</ListGroupItem>
        <ListGroupItem header="Türkçe">{turkish}</ListGroupItem>
        <ListGroupItem header="İspanyolca">{infinitive}</ListGroupItem>
        <ListGroupItem header="İngilizce">{infinitiveEnglish}</ListGroupItem>
        <ListGroupItem header="Birinci Tekil Kişi">{firstPersonSingular}</ListGroupItem>
        <ListGroupItem header="İkinci Tekil Kişi">{secondPersonSingular}</ListGroupItem>
        <ListGroupItem header="Üçüncü Tekil Kişi">{thirdPersonSingular}</ListGroupItem>
        <ListGroupItem header="Birinci Çoğul Kişi">{firstPersonPlural}</ListGroupItem>
        <ListGroupItem header="İkinci Çoğul Kişi">{secondPersonPlural}</ListGroupItem>
        <ListGroupItem header="Üçüncü Çoğul Kişi">{thirdPersonPlural}</ListGroupItem>
      </ListGroup>
  </div>
);
ResultWord.propTypes = {
  turkish: PropTypes.string.isRequired,
  english: PropTypes.string,
  spanish: PropTypes.string,
  date: PropTypes.string,
  examples: PropTypes.array,
  wordID: PropTypes.string,
};

export default ResultWord;
