import React, { Component } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor';
import PropTypes from 'prop-types';

import Conjugations from '../../../../api/Conjugations/Conjugations';
import ResultWord from './ResultWord';

class Results extends Component {
  constructor(props) {
    super(props);
    this.renderWords = this.renderWords.bind(this);
  }
  renderWords() {
    return this.props.results.map((word, index) => (
      <ResultWord
        key={index}
        turkish={word.turkish}
        infinitive={word.infinitive}
        infinitiveEnglish={word.infinitive_english}
        tense={word.tense}
        mood={word.mood}
        firstPersonSingular={word.form_1s}
        secondPersonSingular={word.form_2s}
        thirdPersonSingular={word.form_3s}
        firstPersonPlural={word.form_1p}
        secondPersonPlural={word.form_2p}
        thirdPersonPlural={word.form_3p}
      />));
  }
  render() {
    return (
      <div className="container conjugated__words">
        <div className="row">
          {this.renderWords()}
        </div>
      </div>
    );
  }
}

export default createContainer((props) => {
  Meteor.subscribe('conjugateWords', props.searchValue);
  return {
    results: Conjugations.find({ infinitive: { $regex: props.searchValue, $options: 'i' } }, { skip: 0, limit: 20 }).fetch(),
  };
}, Results);
