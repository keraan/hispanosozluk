import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import Results from './Results';
import './Search.scss';

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchValue: false,
    };
    this.setSearchValue = this.setSearchValue.bind(this);
    this.getResults = this.getResults.bind(this);
  }
  setSearchValue(e) {
    this.setState({
      searchValue: e.target.value,
    }, () => console.log(this.state.searchValue));
  }
  getResults() {
    this.setState({
      getResults: this.state.searchValue
    })
  }
  render() {
    return (
      <div>
        <div>
          <div className="search-container">
            <input
              className="search-bar"
              type="text"
              id="search-bar"
              placeholder="Hangi fiili cekimlemek istiyorsunuz ?"
              onChange={this.setSearchValue}
            />
            <Button onClick={this.getResults}>Çekimle</Button>
          </div>
        </div>
        {this.state.getResults ? <Results searchValue={this.state.getResults} /> : null
        }
      </div>
    );
  }
}

export default Search;
