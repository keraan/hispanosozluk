import React from 'react';
import './Grammar.scss';

const Grammar = () => (
  <div className="container">
    <div className="row">
      <div className="col-lg-8 col-md-8 col-sm-9 col-xs-12">
        <article>
          <div className="row">
            <div className="col-sm-6 col-md-4">
              <figure>
                <img src="http://usbcode.ir/wp-content/uploads/2017/03/bootstrap.jpg" />
              </figure>
            </div>
            <div className="col-sm-6 col-md-8">
              <span className="label label-default pull-right"><i className="glyphicon glyphicon-comment" />50</span>
              <h4>Raymond Dragon</h4>
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
              <section>
                <i className="glyphicon glyphicon-folder-open" />Bootstrap
  		                    <i className="glyphicon glyphicon-user" />RaymondDragon
  		                    <i className="glyphicon glyphicon-calendar" />1395/12/21
  		                    <i className="glyphicon glyphicon-eye-open" />10000
  		                    <a href="#" className="btn btn-default btn-sm pull-right">More ... </a>
              </section>
            </div>
          </div>
        </article>

        <article>
          <div className="row">
            <div className="col-sm-6 col-md-4">
              <figure>
                <img src="http://usbcode.ir/wp-content/uploads/2017/03/bootstrap.jpg" />
              </figure>
            </div>
            <div className="col-sm-6 col-md-8">
              <span className="label label-default pull-right"><i className="glyphicon glyphicon-comment" />50</span>
              <h4>Raymond Dragon</h4>
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
              <section>
                <i className="glyphicon glyphicon-folder-open" />Bootstrap
  		                    <i className="glyphicon glyphicon-user" />RaymondDragon
  		                    <i className="glyphicon glyphicon-calendar" />1395/12/21
  		                    <i className="glyphicon glyphicon-eye-open" />10000
  		                    <a href="#" className="btn btn-default btn-sm pull-right">More ... </a>
              </section>
            </div>
          </div>
        </article>

        <article>
          <div className="row">
            <div className="col-sm-6 col-md-4">
              <figure>
                <img src="http://usbcode.ir/wp-content/uploads/2017/03/bootstrap.jpg" />
              </figure>
            </div>
            <div className="col-sm-6 col-md-8">
              <span className="label label-default pull-right"><i className="glyphicon glyphicon-comment" />50</span>
              <h4>Raymond Dragon</h4>
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
              <section>
                <i className="glyphicon glyphicon-folder-open" />Bootstrap
  		                    <i className="glyphicon glyphicon-user" />RaymondDragon
  		                    <i className="glyphicon glyphicon-calendar" />1395/12/21
  		                    <i className="glyphicon glyphicon-eye-open" />10000
  		                    <a href="#" className="btn btn-default btn-sm pull-right">More ... </a>
              </section>
            </div>
          </div>
        </article>

        <article>
          <div className="row">
            <div className="col-sm-6 col-md-4">
              <figure>
                <img src="http://usbcode.ir/wp-content/uploads/2017/03/bootstrap.jpg" />
              </figure>
            </div>
            <div className="col-sm-6 col-md-8">
              <span className="label label-default pull-right"><i className="glyphicon glyphicon-comment" />50</span>
              <h4>Raymond Dragon</h4>
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
              <section>
                <i className="glyphicon glyphicon-folder-open" />Bootstrap
  		                    <i className="glyphicon glyphicon-user" />RaymondDragon
  		                    <i className="glyphicon glyphicon-calendar" />1395/12/21
  		                    <i className="glyphicon glyphicon-eye-open" />10000
  		                    <a href="#" className="btn btn-default btn-sm pull-right">More ... </a>
              </section>
            </div>
          </div>
        </article>

        <article>
          <div className="row">
            <div className="col-sm-6 col-md-4">
              <figure>
                <img src="http://usbcode.ir/wp-content/uploads/2017/03/bootstrap.jpg" />
              </figure>
            </div>
            <div className="col-sm-6 col-md-8">
              <span className="label label-default pull-right"><i className="glyphicon glyphicon-comment" />50</span>
              <h4>Raymond Dragon</h4>
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
              <section>
                <i className="glyphicon glyphicon-folder-open" />Bootstrap
  		                    <i className="glyphicon glyphicon-user" />RaymondDragon
  		                    <i className="glyphicon glyphicon-calendar" />1395/12/21
  		                    <i className="glyphicon glyphicon-eye-open" />10000
  		                    <a href="#" className="btn btn-default btn-sm pull-right">More ... </a>
              </section>
            </div>
          </div>
        </article>

        <article>
          <div className="row">
            <div className="col-sm-6 col-md-4">
              <figure>
                <img src="http://usbcode.ir/wp-content/uploads/2017/03/bootstrap.jpg" />
              </figure>
            </div>
            <div className="col-sm-6 col-md-8">
              <span className="label label-default pull-right"><i className="glyphicon glyphicon-comment" />50</span>
              <h4>Raymond Dragon</h4>
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
              <section>
                <i className="glyphicon glyphicon-folder-open" />Bootstrap
  		                    <i className="glyphicon glyphicon-user" />RaymondDragon
  		                    <i className="glyphicon glyphicon-calendar" />1395/12/21
  		                    <i className="glyphicon glyphicon-eye-open" />10000
  		                    <a href="#" className="btn btn-default btn-sm pull-right">More ... </a>
              </section>
            </div>
          </div>
        </article>
      </div>
    </div>
  </div>
);

export default Grammar;
