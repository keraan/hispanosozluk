import React from 'react';
import { year } from '@cleverbeagle/dates';
import { Link } from 'react-router-dom';
import { Grid } from 'react-bootstrap';

import './Footer.scss';

const copyrightYear = () => {
  const currentYear = year();
  return currentYear === '2017' ? '2017' : `2017-${currentYear}`;
};

const Footer = () => (
  <div className="Footer">
    <Grid>
      <p className="pull-left">&copy; {copyrightYear()} HispanoSözlük</p>
      <ul className="pull-right">
        <li><Link to="/about">Blog<span className="hidden-xs"></span></Link></li>
        <li><Link to="/about">Hakkında<span className="hidden-xs"></span></Link></li>
        <li><Link to="/about">İletişim</Link></li>
      </ul>
    </Grid>
  </div>
);

Footer.propTypes = {};

export default Footer;
