import React from 'react';
import PropTypes from 'prop-types';
import { FormGroup, ControlLabel, Button, Collapse } from 'react-bootstrap';
import { Meteor } from 'meteor/meteor';
import { Bert } from 'meteor/themeteorchef:bert';
import validate from '../../../modules/validate';

import './WordEditor.scss'

class DocumentEditor extends React.Component {
  constructor(...args) {
    super(...args);
    this.state = {};
  }
  componentDidMount() {
    const component = this;
    validate(component.form, {
      rules: {
        turkish: {
          required: true,
        },
        spanish: {
          required: true,
        },
        english: {
          required: true,
        },
        examples: {
          required: true,
        },
        type: {
          required: true,
        },
        explanation: {
          required: false,
        },
      },
      messages: {
        turkish: {
          required: 'Türkçe bir kelime girin.',
        },
        spanish: {
          required: 'İspanyolca bir kelime girin.',
        },
        english: {
          required: 'İngilizce bir kelime girin.',
        },
        examples: {
          required: 'Bir örnek verin.',
        },
        type: {
          required: 'Kelimenin türünü belirtin.',
        },
        explanation: {
          required: 'Kelime ile ilgili açıklama giriniz.',
        },
      },
      submitHandler() { component.handleSubmit(); },
    });
  }

  handleSubmit() {
    const { history } = this.props;
    const existingDocument = this.props.doc && this.props.doc._id;
    const methodToCall = existingDocument ? 'dictionary.update' : 'dictionary.insert';
    const doc = {
      turkish: this.turkish.value.trim(),
      spanish: this.spanish.value.trim(),
      english: this.english.value.trim(),
      examples: this.examples.value.trim(),
      explanation: this.explanation.value.trim(),
      type: this.wordType.value.trim(),

    };

    if (existingDocument) doc._id = existingDocument;

    Meteor.call(methodToCall, doc, (error, documentId) => {
      if (error) {
        Bert.alert(error.reason, 'danger');
      } else {
        const confirmation = existingDocument ? 'Kelime düzenlendi.!' : 'Kelime eklendi.!';
        this.form.reset();
        Bert.alert(confirmation, 'success');
        history.push(`/words/${documentId}`);
      }
    });
  }

  render() {
    const { doc } = this.props;
    return (
      <form ref={form => (this.form = form)} onSubmit={event => event.preventDefault()}>
      <div className="container">
        <div className="row">
            <div className="col-md-8">
              <FormGroup>
                <ControlLabel>Türkçe</ControlLabel>
                <input
                  type="text"
                  className="form-control"
                  name="turkish"
                  ref={turkish => (this.turkish = turkish)}
                  defaultValue={doc && doc.turkish}
                  placeholder="Kelimeyi Türkçe olarak yazın."
                />
              </FormGroup>
              <FormGroup>
                <ControlLabel>İspanyolca</ControlLabel>
                <input
                  type="text"
                  className="form-control"
                  name="spanish"
                  ref={spanish => (this.spanish = spanish)}
                  defaultValue={doc && doc.spanish}
                  placeholder="Kelimenin İspanyolca karşılığını girin."
                />
              </FormGroup>
              <FormGroup>
                <ControlLabel>İngilizce</ControlLabel>
                <input
                  type="text"
                  className="form-control"
                  name="english"
                  ref={english => (this.english = english)}
                  defaultValue={doc && doc.english}
                  placeholder="Kelimenin İngilizce karşılığını girin."
                />
              </FormGroup>
              <FormGroup>
                <ControlLabel>Örnek</ControlLabel>
                <input
                  type="text"
                  className="form-control"
                  name="examples"
                  ref={examples => (this.examples = examples)}
                  defaultValue={doc && doc.examples}
                  placeholder="Kelimenin kullanımı için bir örnek verin."
                />
              </FormGroup>
              <FormGroup>
                <ControlLabel>Tür</ControlLabel>
                <input
                  type="text"
                  className="form-control"
                  name="wordType"
                  ref={wordType => (this.wordType = wordType)}
                  defaultValue={doc && doc.type}
                  placeholder="Kelimenin türünü girin"
                />
              </FormGroup>

            </div>
            <div className="col-md-4">
              <Button className="explanation__button"
                      bsSize="small" bsStyle="info"
                      onClick={() => this.setState({ open: !this.state.open })} block>
          Açıklama Ekle
              </Button>
              <Collapse in={this.state.open}>
                <div>
                  <FormGroup>
                    <ControlLabel>Açıklama</ControlLabel>
                    <textarea
                      type="text"
                      className="form-control"
                      name="explanation"
                      ref={explanation => (this.explanation = explanation)}
                      defaultValue={doc && doc.explanation}
                      placeholder="Kelimenin türünü girin"
                    />
                  </FormGroup>
                </div>
              </Collapse>
            </div>
        </div>
        <Button type="submit" bsStyle="success">
          {doc && doc._id ? 'Değişiklikleri sakla' : 'Kelime ekle'}
        </Button>
      </div>
    </form>
    );
  }
}

DocumentEditor.defaultProps = {
  doc: { title: '', body: '' },
};

DocumentEditor.propTypes = {
  doc: PropTypes.object,
  history: PropTypes.object.isRequired,
};

export default DocumentEditor;
