import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import Conjugations from '../Conjugations';

Meteor.publish('conjugateWords', (searchValue) => {
  check(searchValue, String);
  return Conjugations.find({ infinitive: searchValue });
});
