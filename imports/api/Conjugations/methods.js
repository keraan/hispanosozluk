import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import Conjugations from './Conjugations';
import rateLimit from '../../modules/rate-limit';

Meteor.methods({
  'conjugations.insert': function ConjugationsInsert(doc) {
    check(doc, {
      title: String,
      body: String,
    });

    try {
      return Conjugations.insert({ owner: this.userId, ...doc });
    } catch (exception) {
      throw new Meteor.Error('500', exception);
    }
  },
  'conjugations.update': function ConjugationsUpdate(doc) {
    check(doc, {
      _id: String,
      title: String,
      body: String,
    });

    try {
      const documentId = doc._id;
      Conjugations.update(documentId, { $set: doc });
      return documentId; // Return _id so we can redirect to document after update.
    } catch (exception) {
      throw new Meteor.Error('500', exception);
    }
  },
  'conjugations.remove': function ConjugationsRemove(documentId) {
    check(documentId, String);

    try {
      return Conjugations.remove(documentId);
    } catch (exception) {
      throw new Meteor.Error('500', exception);
    }
  },
});

rateLimit({
  methods: [
    'conjugations.insert',
    'conjugations.update',
    'conjugations.remove',
  ],
  limit: 5,
  timeRange: 1000,
});
