import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

const Grammar = new Mongo.Collection('Grammar');

Grammar.allow({
  insert: () => false,
  update: () => false,
  remove: () => false,
});

Grammar.deny({
  insert: () => true,
  update: () => true,
  remove: () => true,
});
