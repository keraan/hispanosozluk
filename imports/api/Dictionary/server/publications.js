import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';
import Dictionary from '../Dictionary';

Meteor.publish('findWords', (searchValue, language) => {
  check(searchValue, String);
  check(language, String);
  return language === 'turkish' ? Dictionary.find({ turkish: { $regex: searchValue, $options: 'i' } }) :
    Dictionary.find({ spanish: { $regex: searchValue, $options: 'i' } });
});
Meteor.publish('findWords.mywords', function documents() {
  return Dictionary.find({ owner: this.userId });
});
Meteor.publish('findWords.user', function documentsView(documentId) {
  check(documentId, String);
  return Dictionary.find({ _id: new Meteor.Collection.ObjectID(documentId)});
});
Meteor.publish('findWords.userListView', (word) => {
  check(word, String);
  return Dictionary.find({ turkish: word });
});
