import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';

const Dictionary = new Mongo.Collection('Dictionary');

Dictionary.allow({
  insert: () => false,
  update: () => false,
  remove: () => false,
});

Dictionary.deny({
  insert: () => true,
  update: () => true,
  remove: () => true,
});

Dictionary.schema = new SimpleSchema({
  owner: {
    type: String,
    label: 'The ID of the user this document belongs to.',
  },
  spanish: {
    type: String,
    label: 'The word in Spanish',
  },
  turkish: {
    type: String,
    label: 'The word in Turkish',
  },
  english: {
    type: String,
    label: 'The word in English',
  },
  examples: {
    type: String,
    label: 'Example sentences in Spanish'
  },
  type: {
    type: String,
    label: 'Type of the word'
  },
  explanation: {
    type: String,
    label: 'explanation of the word.'
  },
  createdAt: {
    type: String,
    label: 'The date this document was created.',
    autoValue() {
      if (this.isInsert) return (new Date()).toISOString();
    },
  },
  updatedAt: {
    type: String,
    label: 'The date this document was last updated.',
    autoValue() {
      if (this.isInsert || this.isUpdate) return (new Date()).toISOString();
    },
  },
});

Dictionary.attachSchema(Dictionary.schema);

export default Dictionary;
