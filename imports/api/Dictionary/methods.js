import { Meteor } from 'meteor/meteor';
import { Match, check } from 'meteor/check';
import Dictionary from './Dictionary';
import rateLimit from '../../modules/rate-limit';

Meteor.methods({
  'dictionary.insert': function DictionaryInsert(doc) {
    check(doc, {
      turkish: String,
      spanish: String,
      english: String,
      examples: String,
      explanation: String,
      type: String,
    });

    try {
      return Dictionary.insert({ owner: this.userId, ...doc });
    } catch (exception) {
      throw new Meteor.Error('500', exception);
    }
  },
  'dictionary.update': function DictionaryUpdate(doc) {
    check(doc, {
      _id: Match.Any,
      turkish: String,
      spanish: String,
      english: String,
      examples: String,
      explanation: String,
      type: String,
    });

    try {
      const documentId = doc._id;
      Dictionary.update(documentId, { $set: doc });
      return documentId; // Return _id so we can redirect to document after update.
    } catch (exception) {
      throw new Meteor.Error('500', exception);
    }
  },
  'dictionary.remove': function DictionaryRemove(documentId) {
    check(documentId, String);

    try {
      return Dictionary.remove(documentId);
    } catch (exception) {
      throw new Meteor.Error('500', exception);
    }
  },
});

rateLimit({
  methods: [
    'dictionary.insert',
    'dictionary.update',
    'dictionary.remove',
  ],
  limit: 5,
  timeRange: 1000,
});
